#!/usr/bin/env python

import rospy
from std_msgs.msg import String
from std_msgs.msg import Int32MultiArray
from geometry_msgs.msg import Twist
import time
import struct

conv_value = 0
wheel_radius = 0
wheel_base = 0
linear_velocity = 0
angular_velocity = 0
auger_lift = 0
auger_slide = 0
auger_drive = 0
belt_lift = 0
belt_drive = 0

SERIAL_DELIM = ","
oldstring = "<0,0"



def get_bot_params():
	wheel_radius = rospy.get_param("wheel_radius", 0.138)
	wheel_base = rospy.get_param("wheel_base", 0.751)
	return wheel_radius, wheel_base

def body_to_wheel_speeds(linear_velocity, angular_velocity):
	
	w_left = ( (2*linear_velocity) - (wheel_base*angular_velocity) )/ (2*wheel_radius)
	w_right = ( (2*linear_velocity) + (wheel_base*angular_velocity) )/ (2*wheel_radius)
	return w_left, w_right

def exit_handler():
	"""
	Safe exit of node, releases serial port
	"""
	rospy.loginfo('Closing serial port...')

# callback to store cmd_vel messages to global
# 
def vel_callback(twist):
	global linear_velocity
	global angular_velocity
	linear_velocity = twist.linear.x
	angular_velocity = twist.angular.z
#
# end of function

# callback to store button messages
# 
def btn_callback(btns):
	global auger_lift
	global auger_slide
	global auger_drive
	global belt_lift
	global belt_drive
	
	auger_lift = btns.data[0]
	auger_slide = btns.data[1]
	auger_drive = btns.data[2]
	belt_lift = btns.data[3]
	belt_drive = btns.data[4]
#
# end of function
	
def control():
	"""
	Parses diffdrive_controller's publish_cmd twist message, populates an Int32MultiArray message type after applying math to convert
	body velocities to left and right side wheel velocities.
	"""
	#linear_velocity = Twist.linear.x
	#angular_velocity = Twist.angular.z
	global oldstring

	# movement control variables
	# 
	global linear_velocity
	global angular_velocity

	# auger and belt control variables
	# 
	global auger_lift
	global auger_slide
	global auger_drive
	global belt_lift
	global belt_drive

	w_left, w_right = body_to_wheel_speeds(linear_velocity, angular_velocity)


	# arduino_left = -1* ((w_left - -5.5) * (1024 - -1024) / (5.5 - -5.5) + -1024)
	# arduino_right = -1* ((w_right - -5.5) * (1024 - -1024) / (5.5 - -5.5) + -1024)

	# Convert from radians/second to clicks/second
	# 1024 clicks/rev
	arduino_left = -1 * (w_left * 1024 / (2 * 3.14159))
	arduino_right = -1 * (w_right * 1024 / (2 * 3.14159))

	# Clamp incoming left and right wheel speeds to limit (Limit in Rad/s converted to clicks/s. Clamp limits click/s to theoretical max and min)
	minn = -230 # TODO Bring down for a magnitude for saftey
	maxn = 230 # (Clicks/s theoretical max) # 0.215 m/s, that is 20.64 s to cross 4.44 m
	# If we did maxn and minn of 600 click/s that would be 0.5608 m/s
	arduino_left = minn if arduino_left < minn else maxn if arduino_left > maxn else arduino_left
	arduino_right = minn if arduino_right < minn else maxn if arduino_right > maxn else arduino_right

	
	#arduino_w = [int(arduino_left), int(arduino_right)]
	# serialString = str('<' + str(arduino_w[0]) + ',' + str(arduino_w[1]) + '>')

	# serial format:
	#  <left_motor_speed, right_motor_speed, auger_lift,
	#	auger_slide, auger_drive, belt_lift, belt_drive>
	#
	serialData = [int(arduino_left), int(arduino_right), auger_lift,
				  auger_slide, auger_drive, belt_lift, belt_drive]
	serialString = ("<" + SERIAL_DELIM.join([str(int(token)) for token in
						   serialData]) + ">")

	# serialString = '<' + str(int(arduino_left))  + "," + str(int(arduino_right)) + '>'
	
	if oldstring != serialString:
		rospy.loginfo(serialString)
	oldstring = serialString		
	
	time.sleep(0.05)


def main():
	"""
	Starts subscriber, polls for parameters, and publishes to the /wheel_angular_vel_enc topic
	"""
	global wheel_base
	global wheel_radius

	wheel_radius, wheel_base = get_bot_params()
	rospy.init_node('cmd_to_hardware_debugger', anonymous=True)
	rospy.Subscriber('/cmd_vel', Twist, vel_callback)
	rospy.Subscriber('/btns', Int32MultiArray, btn_callback)
	time.sleep(1)
	
	while not rospy.is_shutdown():
      		control()
	
	rospy.on_shutdown(exit_handler)

if __name__ == '__main__':
	main()
