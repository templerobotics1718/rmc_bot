#!/usr/bin/env python

import rospy
from std_msgs.msg import Float32MultiArray
from geometry_msgs.msg import Twist

w_left = 0
w_right = 0

def body_to_wheel_speeds(linear_velocity, angular_velocity):
    w_left = ( (2*linear_velocity) - (0.53975*angular_velocity) )/ (2*0.275)
    w_right = ( (2*linear_velocity) + (0.53975*angular_velocity) )/ (2*0.275)
    return w_left, w_right

def callback(Twist):
    """
    Parses diffdrive_controller's publish_cmd twist message, populates an Int32MultiArray message type after applying math to convert
    body velocities to left and right side wheel velocities.
    """
    global w_left
    global w_right
    conv_value = 13.75
    #global conv_value
    linear_velocity = Twist.linear.x
    angular_velocity = Twist.angular.z

    w_left, w_right = body_to_wheel_speeds(linear_velocity, angular_velocity)
    #w_left = w_left*conv_value
    #w_right = w_right*conv_value

def main():
    global w_left
    global w_right
    rospy.init_node('cmd_to_hardware', anonymous=True)
    rospy.Subscriber('/cmd_vel', Twist, callback)
    pub = rospy.Publisher('/cmd_vel_wheel', Float32MultiArray, queue_size=100)
    x = Float32MultiArray()
    rate = rospy.Rate(10)

    while not rospy.is_shutdown():
	#publish cm/sec for Arduino on ROS serial
	x.data = [10*w_left,10*w_right]
	pub.publish(x)
        rate.sleep()

if __name__ == '__main__':
    main()

