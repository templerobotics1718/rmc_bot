#!/usr/bin/env python

import rospy
from std_msgs.msg import Int32MultiArray
from geometry_msgs.msg import Twist
from geometry_msgs.msg import Point
import socket
import struct

velocityPub = rospy.Publisher('/cmd_vel', Twist, queue_size = 10)
anglePub = rospy.Publisher('/servos', Point, queue_size = 10)
buttonPub = rospy.Publisher('/btns', Int32MultiArray, queue_size = 10)

def sendJoystickData(joystick):
	#get data
	cmd_vel = Twist()
	angles = Point()
	cmd_vel.linear.x = joystick[1]
	cmd_vel.angular.z = joystick[0]
	#angles.x = joystick[2]
	#angles.y = joystick[3]

	#convert data types
	#velocity = Twist(linear_vel, ang_vel)
	#angles = Point(pan_angle, tilt_angle,0)

	#publish
	velocityPub.publish(cmd_vel)
	#anglePub.publish(angles)

	button_data = []

	for i in xrange(2, len(joystick)):
		button_data.append(joystick[i])

	button_msg = Int32MultiArray()
	button_msg.data = button_data

	buttonPub.publish(button_msg)

def exit_handler():
	rospy.loginfo('Closing Joystick Node')

def shutdown(reason=""):
	rospy.signal_shutdown(reason)
	exit_handler()

def main():
	#init node
	rospy.init_node('JoystickNode',anonymous=True)
	rospy.on_shutdown(exit_handler)


	#init values
	
	###########################################
	# Change UDP port here
	#
	udp_ip = '192.168.0.103'
	udp_port = 5005
	###########################################
	buffer_size = 2048

	#init server
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	s.bind((udp_ip, udp_port))

	# Needs to be non-blocking with a timeout to flush buffer
	s.setblocking(0)
	s.settimeout(0.0)

	try:
		while not rospy.is_shutdown():
			# Receive data until socket buffer is empty
			data = False
			try:
				while True:
					data = s.recv(buffer_size)

			# Empty buffer throws error - be careful not to catch too much!
			except:
				pass
				
			if not data:
				continue

			# 2 floats, 6 bytes, little endian

			try:
			    #################################################
			    # Change this line to adjust for packet structure
			    #################################################
				data = struct.unpack("=ffBBBBBB", data)
			except Exception as error:
				rospy.loginfo(error)
				pass
			if not data: 
				break

			rospy.loginfo(data)

			#parse and send data
			sendJoystickData(data)

	except Exception as e:
		rospy.loginfo(e)
		#rospy.loginfo(error)
	rospy.loginfo("Closing")

####################################################
if __name__ == "__main__":
	main()
