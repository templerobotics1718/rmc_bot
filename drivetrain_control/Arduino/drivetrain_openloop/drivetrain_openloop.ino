#include <Sabertooth.h>
#include <AccelStepper.h>
#include "rmc_stepper.h"

// Front Sabertooth Serial Port
Sabertooth  KF(128,Serial2);

// Back Sabertooth Serial Port
Sabertooth  KB(128,Serial1);

// Sabertooth digging and dumping systems
Sabertooth motors(129, Serial3);
Sabertooth lift(128, Serial3);

// Stepper
// dir pin is 4 pull pin is 5
// common fucking ground that shit
const unsigned char limit_pin = 3;
RmcStepper powerscrew(5, 4, 100.0f);
bool stepper_on = false;
long pause = -1;
long old_pause = -1;

// Motor speeds (for auger drive 1 is digging direction)
const char NUM_MOTOR_SPEEDS = 3;
const int MOTOR_SPEEDS[3] = {0, 127, -127};
const int BELT_SPEEDS[3] = {0, -127, 127};

// Type declarations
struct DriveMessage {
  long left_speed, right_speed;
  long auger_lift, auger_slide, auger_drive;
  long belt_lift, belt_drive;
};
struct FeedbackMessage {
  long left_front_speed, left_back_speed, right_front_speed, right_back_speed, stepper_pos;
};

// Serial communication buffers
const unsigned int BUFFER_SIZE = 256;
char message_buffer[BUFFER_SIZE];
char feedback_buffer[BUFFER_SIZE];

// Control variables
long left_speed = 0, right_speed = 0;
const unsigned int MAX_SPEED_CHANGE = 50; //orginally 40
const unsigned int DELAY_TIME = 10;
DriveMessage message;

void handle_message() {
  char* last_token = strtok(message_buffer, "<");
  char* next_token;

  // Consider cleaning this section up, maybe switch back to reinterpret casting
  // a packed struct?

  // left speed, right speed, char [auger lift, auger slide, auger drive, belt lift, belt drive]
  {
    message.left_speed = strtol(last_token, &next_token, 10);
    last_token = next_token + 1;
    message.right_speed = strtol(last_token, &next_token, 10);
    last_token = next_token + 1;
    message.auger_lift = strtol(last_token, &next_token, 10);
    last_token = next_token + 1;
    message.auger_slide = strtol(last_token, &next_token, 10);
    last_token = next_token + 1;
    message.auger_drive = strtol(last_token, &next_token, 10);
    last_token = next_token + 1;
    message.belt_lift = strtol(last_token, &next_token, 10);
    last_token = next_token + 1;
    message.belt_drive = strtol(last_token, &next_token, 10);
  }
}

void update_motors() {
  left_speed = constrain(message.left_speed, left_speed - MAX_SPEED_CHANGE, left_speed + MAX_SPEED_CHANGE);
  right_speed = constrain(message.right_speed, right_speed - MAX_SPEED_CHANGE, right_speed + MAX_SPEED_CHANGE);
  //left_speed = message.left_speed;
  //right_speed = message.right_speed;

  KF.motor(1,left_speed);
  KF.motor(2,right_speed);
  KB.motor(1,left_speed);
  KB.motor(2,right_speed);

  //0 = STOP, 1 = UP, 2 = DOWN
  //Serial.println(message.auger_lift);
  if (message.auger_lift >= 0 && message.auger_lift < NUM_MOTOR_SPEEDS) {
    lift.motor(1, MOTOR_SPEEDS[message.auger_lift]);
  }

  if (message.belt_lift >= 0 && message.belt_lift < NUM_MOTOR_SPEEDS) {
    lift.motor(2, MOTOR_SPEEDS[message.belt_lift]);
  }

  if (message.belt_drive >= 0 && message.belt_drive < NUM_MOTOR_SPEEDS) {
    motors.motor(1, BELT_SPEEDS[message.belt_drive]);
  }

  if (message.auger_drive >= 0 && message.auger_drive < NUM_MOTOR_SPEEDS) {
    motors.motor(2, MOTOR_SPEEDS[message.auger_drive]);
  }

  switch (message.auger_slide)
  {
    case 0:
      // stop
      pause = powerscrew.getCurrentPos();
      powerscrew.stop();
      break;
    case 1:
      // Move down track (forward)
      if (old_pause != pause){
        powerscrew.setCurrentPos(pause);
      }
      powerscrew.setGoalPos(-78000);
      old_pause = pause;
      powerscrew.start();
      break;
    case 2:
      // Move up track (backward)
      // this will set the GoalPos to tbd
      if (old_pause != pause){
        powerscrew.setCurrentPos(pause);
      }
      powerscrew.setGoalPos(-2);
      old_pause = pause;
      powerscrew.start();
      break;
  }
}


void setup() {
  Serial.begin(9600);
  Serial1.begin(9600);
  Serial2.begin(9600);
  Serial3.begin(9600);
  //lift.autobaud();
  motors.autobaud();
  KF.autobaud();
  KB.autobaud();
  //Serial.println("Starting");

  // Stepper Init (home top to zero)
  pinMode(limit_pin, INPUT_PULLUP);
  long initial_homing = 400;

//Serial.println("Stepper is Homing...");
  while (digitalRead(limit_pin)) {
    //Serial.println("in 1st loop");
    powerscrew.setGoalPos(initial_homing);
    initial_homing = initial_homing + 400;
    powerscrew.start();
    delay(5);
  }

  powerscrew.setCurrentPos(0);
  initial_homing = -400;

  while (!digitalRead(limit_pin)) {
    //Serial.println("in 2nd loop");
    powerscrew.setGoalPos(initial_homing);
    powerscrew.start();
    initial_homing = initial_homing - 400;
    delay(5);
  }
  powerscrew.stop();
  powerscrew.setCurrentPos(0);
  //Serial.println("Homing Complete");
}

void loop() {
  // Read all serial data up to end of packet
  if (Serial.available() > 0) {
    unsigned int message_length = Serial.readBytesUntil('>', message_buffer, 256);
    if (message_length > 0) {
      message_buffer[message_length] = '\0';
      handle_message();
    }
  }
  update_motors();
  delay(10);
}
