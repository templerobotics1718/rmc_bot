#!/usr/bin/env python
import rospy
from std_msgs.msg import Int32MultiArray

def main():

    rospy.init_node("jenky_btns")
    
    pub = rospy.Publisher("/btns", Int32MultiArray)
    rospy.loginfo("Starting jenky button control...")
    rospy.loginfo("Issue commands using element_num=command")
    rospy.loginfo("Example to raise auger: 0=1")
    rospy.loginfo("ELEMENTS:\n" +
                  "0:auger lift\n" +
                  "1:auger slide\n" +
                  "2:auger drive\n" +
                  "3:belt lift\n" +
                  "4:belt drive\n")

    rospy.loginfo("COMMANDS:\n" +
        	  "0:stop\n" +
                  "1:raise\n" +
                  "2:lower\n")

    command = [0, 0, 0, 0, 0]
    while not rospy.is_shutdown():
	ctrl = raw_input("element=command: ")
	try:
            if ctrl.lower() == "stop":
                command = [0,0,0,0,0]
	    	msg.data=command
	    	pub.publish(msg)
                continue

	    elem, cmd = ctrl.split("=")
	    elem = int(elem.strip())
	    cmd = int(cmd.strip())
	    
	    if elem < 0 or elem > 4:
		rospy.logwarn("improper element value. must be between 0 and 4 (inclusive)")
		continue
	    
	    if cmd < 0 or cmd > 2:
		rospy.logwarn("improper command value. must be between 0 and 2 (inclusive)")
		continue
	    
	    command[elem] = cmd
	    msg = Int32MultiArray()
	    msg.data=command
	    pub.publish(msg)

	except:
	    rospy.logwarn("unable to parse command")
            continue

    #
    # end of while

if __name__ == "__main__":
    main()
