#!/usr/bin/env python

import subprocess
import socket
import struct
import os
import signal
import pickle

# Variables for Socket Server
TCP_IP = '192.168.0.103'
TCP_PORT = 5006
BUFFER_SIZE = 4096

# Global variable for subprocess Popen object
popen_object = None


# Function takes in commands from RUIN and spins or kills threads. Uses socket connection object to communicate to RUIN
# 0 = Killed
# 1 = Spawned
# 3 = Recieved command is invalid
def runScript(cmd, sock):
    global popen_object

    if len(cmd) != 2:
        print "Malformatted startup command"
        return

    # Parse commands
    command = cmd[0]
    openOrClose = cmd[1][0]

    # If command is auto, check to see if it needs spawned or killed
    if command == 'auto':
        if openOrClose == '1':
            popen_object = subprocess.Popen(['roslaunch', 'simbot_description', 'diffdrive.launch'])
            print("Test")
            # Send Confirmation it started
            #sock.send("1")
            # Kill autonomous 
        elif openOrClose == '0':
            if popen_object is not None:
                popen_object.terminate()
                #sock.send("0")
                print("Killed Auto")
                # Ensure Popen object is empty for next round of commands
                popen_object = None
            else:
                print("auto is not running")
    elif command == 'nona':
        if openOrClose == '1':
            popen_object = subprocess.Popen(['roslaunch', 'drivetrain_control', 'teleop.launch'])
            print("Launching Teleop Control")
            #sock.send("1")
        elif openOrClose == '0':
            if popen_object is not None:
                popen_object.terminate()
                #sock.send("0")
                print("Killed Teleop")
                popen_object = None
            else:
                print("manual is not running")
    else:
        print("Input is Invalid")
        #sock.send("3")
def main():


    # Startup the Server
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind( (TCP_IP, TCP_PORT))
    s.listen(1)

    conn, addr = s.accept()

    try:
        while True:
            data = conn.recv(BUFFER_SIZE)
            # Deserliazation message
            data = data.split(',')

            if data is not None:
                runScript(data,conn)
            else:
                print('Client disconnected')
                break
    except Exception as e:
        print(e)
        conn.close()
        exit()
                  
if __name__ == "__main__":
    main()
