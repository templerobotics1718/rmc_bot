#include "rmc_stepper.h"
#include <Stepper.h>
RmcStepper powerscrew(5, 4, 100.0f);
const unsigned char limit_pin = 3;

void setup() {
  Serial.begin(9600);

  // Stepper Init
  pinMode(limit_pin, INPUT);
  long initial_homing = 400;
  
  Serial.println("Stepper is Homing...");
  while (digitalRead(limit_pin)) {
    Serial.println("in 1st loop");
    powerscrew.setGoalPos(initial_homing);
    initial_homing = initial_homing + 400;
    powerscrew.start();
    delay(5);
  }
  
  powerscrew.setCurrentPos(0);
  initial_homing = -400;

  while (!digitalRead(limit_pin)) {
    Serial.println("in 2nd loop");
    powerscrew.setGoalPos(initial_homing);
    powerscrew.start();
    initial_homing = initial_homing - 400;
    delay(5);
  }
  powerscrew.stop();
  powerscrew.setCurrentPos(0);
  Serial.println("Homing Complete");

  powerscrew.setCurrentPos(0);
  powerscrew.setGoalPos(-78000);
  powerscrew.start();
  //powerscrew.stop();
  powerscrew.setGoalPos(-2);
  powerscrew.stop();
}

void loop() {
  //powerscrew.setGoalPos(-4000.0f * 2.0f); //this should go 10 in?
  //powerscrew.start();

  Serial.println(powerscrew.getCurrentPos());
//  powerscrew.setGoalPos(-78000);
//  powerscrew.start();
  //Serial.println(digitalRead(limit_pin));
  
  //test.step(-1000); //this is 1.75 inches down the track. so 1 step 0.00005341 in per step
  //need to go 22 in down the track from home...so 411908 steps are needed from home
  //spencers non-blocking library should be able to handle this from here...
  //stepper lib can at most do 32767 steps without an overflow
  //so can do 12 big steps, leaving 18704 steps to go
  //delay(10000);
}


