#!/usr/bin/env python
import rospy
import socket
from nav_msgs.msg import Odometry
from geometry_msgs.msg import *
from tf.transformations import euler_from_quaternion
import struct

UDP_IP = "192.168.0.100"
UDP_PORT = 5005


def sendingUDP(msg):
    x = msg.pose.pose.position.x
    y = msg.pose.pose.position.y
    (roll, pitch, yaw) = euler_from_quaternion([msg.pose.pose.orientation.x, msg.pose.pose.orientation.y, msg.pose.pose.orientation.z, msg.pose.pose.orientation.w])

    
    lx = msg.twist.twist.linear.x
    ly = msg.twist.twist.linear.y
    ax = msg.twist.twist.angular.x
    ay = msg.twist.twist.angular.y
    az = msg.twist.twist.angular.z
    
    qx = msg.pose.pose.orientation.x
    qy = msg.pose.pose.orientation.y
    qz = msg.pose.pose.orientation.z
    qw = msg.pose.pose.orientation.w



    # stringMsg = struct.pack('=ffffffffff', x, y, roll, pitch, yaw, lx, ly, ax, ay, az)
    struct.pack('=fffffffffff', x, y, qx, qy, qz, qw, lx, ly, ax, ay, az)

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.sendto(stringMsg, (UDP_IP, UDP_PORT))

def pubsub():
    rospy.init_node('udp', anonymous=True)
    rospy.Subscriber('/odom', Odometry, sendingUDP)
    rospy.spin()

if __name__ == '__main__':
    try:
        pubsub()
    except rospy.ROSInterruptException:
        pass
    
